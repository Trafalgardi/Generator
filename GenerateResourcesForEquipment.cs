﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.DataClasses;
using TestProject.Table;

namespace TestProject
{
    public class GenerateResourcesForEquipment
    {
        public List<List<int>> NewGenerate(DataCoefs dataCoefs, List<int> rowsAmount, List<Cell> cells, List<int>  columnsAmount)
        {
            var calculatedForColor = GenerateAllCount(rowsAmount, cells, columnsAmount);
            return calculatedForColor;
        }

        private Random _random = new();
        public List<List<int>> GenerateAllCount(List<int> rowsAmount, List<Cell> listNumber, List<int> columnsAmount) 
        {
            var listTable = CreateTwoDimensionalList(rowsAmount, listNumber);

            for (var i = rowsAmount.Count-1; i >= 0; i--)
            {
                var row = Enumerable.Repeat(0,listNumber.Count).ToList();

                while (rowsAmount[i] != 0)
                {
                    var count = listNumber.Count;

                    bool isAnyPriceNotZero = true; 
                    
                    while (rowsAmount[i] > 5 && isAnyPriceNotZero)
                    {
                        isAnyPriceNotZero = false;
                        
                        for (var j = count - 1; j >= count / 2; j--)
                        {
                            var price = listNumber[j].Price;
                            
                            if (price != 10000)
                                isAnyPriceNotZero = true;
                            
                            if (price == 10000) continue;
                            if (price == 5 && rowsAmount[i] < 5) continue;
                            if (columnsAmount[j] == 0) continue;
                        
                            var maxValue = Math.Min(columnsAmount[j] / price, rowsAmount[i] / price);
                            var value = _random.Next(0, maxValue + 1);
                        
                            row[j]+= value;
                            columnsAmount[j] -= value * price;
                            rowsAmount[i] -= value * price;
                        }

                        var sum = 0;
                        for (var j = count - 1; j >= count / 2; j--)
                            sum += columnsAmount[j];
                        
                        if (sum == 0)
                            break;
                    }

                    while (rowsAmount[i] > 0)
                    {
                        for (var j = count / 2 - 1; j >= 0; j--)
                        {
                            var price = listNumber[j].Price;
                            if (price == 10000) continue;
                            if (columnsAmount[j] == 0) continue;

                            var maxValue = Math.Min(columnsAmount[j], rowsAmount[i]);
                            var value = _random.Next(0, maxValue + 1);

                            row[j] += value;
                            columnsAmount[j] -= value * price;
                            rowsAmount[i] -= value * price;
                        }
                    }
                }
                
                listTable[i] = row;
            }
            
            return listTable;
        }

        private List<List<int>> CreateTwoDimensionalList(List<int> rowsAmount, List<Cell> cells)
        {
            var result = new List<List<int>>();
            for (int i = 0; i < rowsAmount.Count; i++)
            {
                var listRow = Enumerable.Repeat(0,cells.Count).ToList();
                result.Add(listRow);
            }

            return result;
        }
    }

}