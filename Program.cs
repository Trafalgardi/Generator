﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TestProject.DataClasses;
using TestProject.DB;
using TestProject.Table;

namespace TestProject
{
    public class Program
    {
        private static Data Data = new();
        private static readonly DataCells _dataCells = new();
        private static GenerateResourcesForEquipment _generate = new();
        private static int _step = 5;
        static void Main(string[] args)
        {
            //var fileInfo = new FileInfo(@"D:/!CSV/Ресурсы_для_улучшения_экипировки_2.xlsx");
            //var newData = ParseDataFromExcel(fileInfo);
            //var json = JsonSerializer.Serialize(newData);
            //File.WriteAllText($"{Directory.GetCurrentDirectory()}/data.json", json, Encoding.Unicode);
            
            var dataString = File.ReadAllText($"{Directory.GetCurrentDirectory()}/data.json");
            var newData = JsonSerializer.Deserialize<List<DataCoefs>>(dataString);

            var excelSheets = new List<ExcelSheet>();
            for (int i = 0; i < newData.Count; i++)
            {
                var excelSheet = new ExcelSheet();
                excelSheet.Name = newData[i].Name;
                var result = new List<List<int>>();
 
                foreach (var equipmentColor in (EquipmentColor[]) Enum.GetValues(typeof(EquipmentColor)))
                {
                    result.Clear();
                    if (equipmentColor == EquipmentColor.Unknown) continue;
                    
                    var cellCount = Data.CellsCount[equipmentColor];
                    var cells = newData[i].CellCoef[equipmentColor];
                    var cellsAmount = new List<int>();
                    var colorCoef3 = Data.ColorCoef3[equipmentColor];
                    var amount = 0;
                    var firstRow = new List<int>();
                    
                    for (int j = 0; j < cells.Count; j++)
                    {
                        amount += cells[j].Number * cells[j].Price;
                        cellsAmount.Add(cells[j].Number * cells[j].Price * Data.ColorCoef3[equipmentColor]);
                        firstRow.Add(cells[j].Number);
                    }
                    
                    var totalAmount = amount * colorCoef3;
                    
                    var sf = CheckSum(totalAmount, cellCount, _step);
                    while (sf <= 0)
                    {
                        sf = CheckSum(totalAmount, cellCount, _step--);
                    }
                    
                    var ranging = Ranging(sf, totalAmount, cellCount, _step);
                    if (equipmentColor == EquipmentColor.Green) _ = 0;
                    var temp = _generate.NewGenerate(newData[i], ranging, newData[i].CellCoef[equipmentColor], cellsAmount);
                    
                    result.Add(firstRow);
                    result.AddRange(temp);
                    
                    
                    for (int j = 0; j < result.Count; j++)
                    {
                        var dataByColor = new TableElement();
                        dataByColor.color = equipmentColor;
                        for (int k = 0; k < result[j].Count; k++)
                        {
                            var cell = new Cell
                            {
                                Number = result[j][k], Price = cells[k].Price, ResourceType = cells[k].ResourceType
                            };
                            dataByColor.cells.Add(cell);
                        }
                        excelSheet.SheetData.Add(dataByColor);
                    }
                }
                excelSheets.Add(excelSheet);
            }

            var reportExcel = new MarketExcelGenerator();
            reportExcel.ReWriteExcel(excelSheets);
        }

        public static List<DataCoefs> ParseDataFromExcel(FileInfo fileInfo)
        {
            ExcelPackage package = new ExcelPackage(fileInfo);
            //Add the Content sheet
            var worksheet = package.Workbook.Worksheets[0];

            var indexRow = 33;
            for (int i = 0; i < _dataCells.allCellCoef.Count; i++)
            {
                foreach (var color in (EquipmentColor[]) Enum.GetValues(typeof(EquipmentColor)))
                {
                    var indexColumn = 3;
                    if (color == EquipmentColor.Unknown) continue;

                    for (int j = 0; j < 8; j++)
                    {
                        var excelRange = worksheet.Cells[indexRow, indexColumn];
                        int number = 0;
                        if (excelRange.Value != null)
                        {
                            number = Int32.Parse(excelRange.Value.ToString());
                        }

                        var resourceType = _dataCells.allCellCoef[i].CellCoef[color][j].ResourceType;
                        if (resourceType == ResourcesId.FirePowder ||
                            resourceType == ResourcesId.WispsWind ||
                            resourceType == ResourcesId.SilveryWater ||
                            resourceType == ResourcesId.SoulsStones)
                        {
                            _dataCells.allCellCoef[i].CellCoef[color][j].Price = number != 0 ? 1 : 10000;
                        }

                        if (resourceType == ResourcesId.FrozenFlame ||
                            resourceType == ResourcesId.LoopingSmoke ||
                            resourceType == ResourcesId.ScatteringDenseFog ||
                            resourceType == ResourcesId.NonSolidifyingMagma)
                        {
                            _dataCells.allCellCoef[i].CellCoef[color][j].Price = number != 0 ? 5 : 10000;
                        }

                        _dataCells.allCellCoef[i].CellCoef[color][j].Number = number;
                        indexColumn++;
                    }

                    indexRow++;
                }
            }

            return _dataCells.allCellCoef;
        }

        public static bool ArithmeticProgressCheck(int startFrom, int n, int count)
        {
            var temp = n - 1 + startFrom;
            var result = n * (startFrom + temp) / 2;
            if (count < result)
                return false;
            return true;
        }
        
        public static int ArithmeticProgress(int startFrom, int n, int step = 1)
        {
            var result = n * (2 * startFrom + step*(n - 1)) / 2;
            return result;
        }

        public static List<int> Ranging(int sf, int amount, int cellsCount, int step = 1)
        {
            var result = new List<int>();
            var arithmeticProgress = ArithmeticProgress(sf, cellsCount, step);
            var remainder = amount - arithmeticProgress;
            
            while (result.Count < cellsCount)
            {
                result.Add(sf);
                sf+=step;
            }
            Console.WriteLine($"{amount} : {sf} : {cellsCount} : {arithmeticProgress}");
            return DistributionResidues(result, remainder);
        }

        public static List<int> DistributionResidues(List<int> ranging, int remainder)
        {
            //TODO: плохой код, нужен рефактор
            if (remainder <= 0) return ranging;
            if (remainder <= ranging.Count)
            {
                for (int i = ranging.Count - 1; i >= 0; i--)
                {
                    if (remainder <= 0)
                        break;
                    ranging[i]++;
                    remainder--;
                } 
            }
            else
            {
                var circle = remainder / ranging.Count;
                do
                {
                    for (int i = ranging.Count - 1; i >= 0; i--)
                    {
                        if (remainder <= 0)
                            break;
                        ranging[i]++;
                        remainder--;
                    }
                    circle--;
                } while (circle >= 0 || remainder > 0);
            }
            return ranging;
        }

        public static int CheckSum(int amount, int n, int step = 1)
        {
            //var sf1 = (2 * amount - step * (n * n - n)) / (2 * n);
            //var sf2 = (2 * amount / n - n + 1) / 2;
            var sf = (-step*(n*n) + step*n + 2* amount) / (2 * n);
            return sf;
        }

        private static void WriteLog(int numberOfPoints, List<Cell> cells, EquipmentColor color)
        {
            if (numberOfPoints == cells.Sum(singleCell => singleCell.Number * singleCell.Price))
                Console.WriteLine($"\nВсе отлично {color} {numberOfPoints}");
            else
            {
                Console.WriteLine(
                    "\n_________________________________________________________________________________________");
                Console.WriteLine($"\nВсе плохо {color} {numberOfPoints}");
                Console.WriteLine("\nСписок ячеек:");
                foreach (var singleCell in cells)
                {
                    Console.WriteLine(
                        $"Type: {singleCell.ResourceType} | Price: {singleCell.Price} | Number: {singleCell.Number}");
                }

                Console.WriteLine($"\nБыло очков для распеределения: {numberOfPoints}");
                Console.WriteLine(
                    $"\nРасспеределено очков: {cells.Sum(singleCell => singleCell.Number * singleCell.Price)}");
            }
        }
    }

    public class MarketExcelGenerator
    {
        public static Data Data = new();
        
        public void ReWriteExcel(List<ExcelSheet> table)
        {
            using (var controller = new ResourcesToUpgradeEquipmentController())
            {
                controller.DeleteAll();
                
                var eqBase = 1;
                for (var index = 0; index < table.Count; index++)
                //for (var index = 0; index < 1; index++)
                {
                    var sheet = table[index];
                    var nums = new List<int>(8);

                    var eqSharpening = 0;
                    var eqColor = 0;
                    foreach (var tableElement in sheet.SheetData)
                    {
                        var newEqColor = (int) tableElement.color;
                        if (eqColor != newEqColor)
                        {
                            eqSharpening = 0;
                            eqColor = newEqColor;
                        }

                        nums.Clear();
                        nums.AddRange(tableElement.cells.Select(c => c.Number));

                        var record = new ResourcesToUpgradeEquipment();
                        record.Set(eqColor, eqSharpening, eqBase,
                            nums[0], nums[1], nums[2],
                            nums[3], nums[4], nums[5],
                            nums[6], nums[7], -999);

                        controller.AddRecord(record);

                        eqSharpening++;
                    }

                    eqBase++;
                }
            }

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            //var package = new ExcelPackage(new FileInfo(@"D:/!CSV/Ресурсы_для_улучшения_экипировки_генератор.xlsx"));
            var package = new ExcelPackage(new FileInfo(@"Q:\eX\Projects\MMO\Generator\Data\Ресурсы_для_улучшения_экипировки_генератор.xlsx"));

            for (int q = 0; q < table.Count; q++)
            {

                var sheet = package.Workbook.Worksheets.First(n => n.Name == $"{table[q].Name}");

                var color = EquipmentColor.Unknown;
                for (int j = 0; j < table[q].SheetData.Count; j++)
                {
                    if (j < table[q].SheetData[j].cells.Count)
                        sheet.Cells[1, 2 + j].Value = table[q].SheetData[0].cells[j].ResourceType;
                    sheet.Cells[2 + j, 1].Value = $"{table[q].SheetData[j].color}";
                    if (color != table[q].SheetData[j].color)
                    {
                        color = table[q].SheetData[j].color;
                        sheet.Cells[j + 2, 1, j + 2, 10].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    }

                    var sum = 0;
                    for (int i = 0; i < table[q].SheetData[j].cells.Count; i++)
                    {
                        sheet.Cells[2 + j, i + 2].Value = table[q].SheetData[j].cells[i].Number;
                        if (table[q].SheetData[j].cells[i].Price == 10000)
                            continue;
                        sum += table[q].SheetData[j].cells[i].Price * table[q].SheetData[j].cells[i].Number;
                    }

                    sheet.Cells[2 + j, 10].Value = sum;
                    sheet.Cells[2 + j, 11].Value = $"Amount = {sum * Data.ColorCoef3[table[q].SheetData[j].color]}";
                }

                foreach (var equipmentColor in (EquipmentColor[]) Enum.GetValues(typeof(EquipmentColor)))
                {
                    if (equipmentColor == EquipmentColor.Unknown) continue;
                    var cells = sheet.Cells[2, 1, table[q].SheetData.Count + 1, 1]
                        .Where(n => n.Value.ToString() == equipmentColor.ToString()).ToList();
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Merge = true;
                    var first = cells.First().Address.Replace("A", "K");
                    var last = cells.Last().Address.Replace("A", "K");
                    sheet.Cells[$"{first}:{last}"].Merge = true;
                    
                    var cellColor = Color.White;
                    if (equipmentColor != EquipmentColor.Omnicolor)
                        cellColor = Color.FromKnownColor(Enum.Parse<KnownColor>(equipmentColor.ToString()));
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Style.Fill.SetBackground(cellColor);
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Style.VerticalAlignment =
                        ExcelVerticalAlignment.Center;
                }
            }
            Console.WriteLine("ReWrite File...");
            package.Save();
        }
        public void WriteExcel(List<ExcelSheet> table)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            //var package = new ExcelPackage();
            var package = new ExcelPackage(new FileInfo(@"D:/!CSV/Table.xlsx"));

            for (int q = 0; q < table.Count; q++)
            {
                var sheet = package.Workbook.Worksheets.Add($"{table[q].Name}");
                sheet.DefaultColWidth = 18;
                sheet.Column(2).Style.Font.Color.SetColor(Color.Firebrick);
                sheet.Column(3).Style.Font.Color.SetColor(Color.Orange);
                sheet.Column(4).Style.Font.Color.SetColor(Color.CornflowerBlue);
                sheet.Column(5).Style.Font.Color.SetColor(Color.DarkGreen);
                sheet.Column(6).Style.Font.Color.SetColor(Color.MediumPurple);
                sheet.Column(7).Style.Font.Color.SetColor(Color.Gray);
                sheet.Column(8).Style.Font.Color.SetColor(Color.RoyalBlue);
                sheet.Column(9).Style.Font.Color.SetColor(Color.Plum);
                sheet.Cells[1, 1, table[q].SheetData.Count + 1, 1].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                sheet.Cells[1, 1, table[q].SheetData.Count + 1, 1].Style.HorizontalAlignment =
                    ExcelHorizontalAlignment.Center;

                sheet.Cells[1, 1, 1, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                var color = EquipmentColor.Unknown;
                for (int j = 0; j < table[q].SheetData.Count; j++)
                {
                    if (j < table[q].SheetData[j].cells.Count)
                        sheet.Cells[1, 2 + j].Value = table[q].SheetData[0].cells[j].ResourceType;
                    sheet.Cells[2 + j, 1].Value = $"{table[q].SheetData[j].color}";
                    if (color != table[q].SheetData[j].color)
                    {
                        color = table[q].SheetData[j].color;
                        sheet.Cells[j + 2, 1, j + 2, 10].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    }

                    var sum = 0;
                    for (int i = 0; i < table[q].SheetData[j].cells.Count; i++)
                    {
                        sheet.Cells[2 + j, i + 2].Value = table[q].SheetData[j].cells[i].Number;
                        if (table[q].SheetData[j].cells[i].Price == 10000)
                            continue;
                        sum += table[q].SheetData[j].cells[i].Price * table[q].SheetData[j].cells[i].Number;
                    }

                    sheet.Cells[2 + j, 10].Value = sum;
                }

                foreach (var equipmentColor in (EquipmentColor[]) Enum.GetValues(typeof(EquipmentColor)))
                {
                    if (equipmentColor == EquipmentColor.Unknown) continue;
                    var cells = sheet.Cells[2, 1, table[q].SheetData.Count + 1, 1]
                        .Where(n => n.Value.ToString() == equipmentColor.ToString()).ToList();
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Merge = true;
                    var cellColor = Color.White;
                    if (equipmentColor != EquipmentColor.Omnicolor)
                        cellColor = Color.FromKnownColor(Enum.Parse<KnownColor>(equipmentColor.ToString()));
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Style.Fill.SetBackground(cellColor);
                    sheet.Cells[$"{cells.First().Address}:{cells.Last().Address}"].Style.VerticalAlignment =
                        ExcelVerticalAlignment.Center;
                }
            }
            
            Console.WriteLine("Write file...");
            
            File.WriteAllBytes(
                $"D:/!CSV/Table{DateTime.Now.Day}{DateTime.Now.Hour}{DateTime.Now.Second}{DateTime.Now.Millisecond}.xlsx",
                package.GetAsByteArray());
        }
    }
}