﻿namespace TestProject.Table
{
    public class Cell
    {
        public ResourcesId ResourceType { get; set; }
        public int Price { get; set; }
        public int Number { get; set; }

        public Cell()
        {
            
        }
        public Cell(ResourcesId resourceType, int price, int number)
        {
            ResourceType = resourceType;
            Price = price;
            Number = number;
        }
    }
}