﻿using System.Collections.Generic;

namespace TestProject.Table
{
    public class TableElement
    {
        public List<Cell> cells = new();
        public EquipmentColor color;
    }
}