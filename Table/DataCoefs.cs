using System.Collections.Generic;

namespace TestProject.Table
{
    public class DataCoefs
    {
        public string Name { get; set; }
        public EquipmentSlot EquipmentSlot { get; set; }
        public Dictionary<EquipmentColor, List<Cell>> CellCoef { get; set; }

        public DataCoefs()
        {
            
        }
        public DataCoefs(string name, Dictionary<EquipmentColor, List<Cell>> cellCoef, EquipmentSlot equipmentSlot)
        {
            CellCoef = cellCoef;
            Name = name;
            EquipmentSlot = equipmentSlot;
        }
    }
}