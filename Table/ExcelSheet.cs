using System.Collections.Generic;

namespace TestProject.Table
{
    public class ExcelSheet
    {
        public string Name { get; set; }
        public List<TableElement> SheetData { get; set; }

        public ExcelSheet()
        {
            SheetData = new List<TableElement>();
        }
        public ExcelSheet(string name, List<TableElement> sheetData)
        {
            Name = name;
            SheetData = sheetData;
        }
    }
}