﻿using System.Collections.Generic;
using TestProject.Table;

namespace TestProject.DataClasses
{
    public class DataCells
    {
        private Dictionary<EquipmentColor, List<Cell>> RinoCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 8),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 5),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 11),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 15),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 10),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 13),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 9),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 11),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 4),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 5),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 13),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 7),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 17),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 11),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> SokolCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 5),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 16),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 10),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 13),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 8),
                
                new(ResourcesId.FrozenFlame, 5, 3),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 16),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 4),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 18),
                new(ResourcesId.WispsWind, 1, 11),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 5, 6),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 13),
                
                new(ResourcesId.FrozenFlame, 5, 10),
                new(ResourcesId.LoopingSmoke, 5, 3),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> ButterflyCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 5),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 7),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 10),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 13),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 11),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 9),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 13),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 13),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 3),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 17),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 5, 3),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 4),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 17),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 5, 6),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 7),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> CobraCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 13),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 17),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 9),
                new(ResourcesId.WispsWind, 1, 11),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 14),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 13),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 4),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 16),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 5),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 10),
                new(ResourcesId.WispsWind, 1, 25),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 6),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 12),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> DolphinCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 2),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 2),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 10),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 13),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 3),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 14),
                new(ResourcesId.SoulsStones, 1, 14),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 4),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 17),
                new(ResourcesId.SoulsStones, 1, 15),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 5),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 16),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 9),
                new(ResourcesId.NonSolidifyingMagma, 5, 4),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> FoxCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 2),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 2),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 7),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 1, 1),
                new(ResourcesId.SilveryWater, 1, 16),
                new(ResourcesId.SoulsStones, 1, 16),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 14),
                new(ResourcesId.SoulsStones, 1, 14),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 5),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 1, 17),
                new(ResourcesId.SoulsStones, 1, 17),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 3),
                new(ResourcesId.NonSolidifyingMagma, 5, 3),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 18),
                new(ResourcesId.SoulsStones, 1, 18),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 5, 5),
                new(ResourcesId.NonSolidifyingMagma, 5, 5),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> OuterwearCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 11),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 7),
                new(ResourcesId.SoulsStones, 1, 15),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 13),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 5),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 16),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 11),
                new(ResourcesId.SoulsStones, 1, 18),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 13),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 5, 4),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 11),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 1, 12),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 5, 6),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 18),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 5, 8),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> UnderwearCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 11),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 8),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 10),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 12),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 1, 13),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 5, 3),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 14),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 5, 5),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> FootwearCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 11),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 11),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 3),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 17),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 5),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> RingRightCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 10),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 1),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 1),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 8),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 1),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 10),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 11),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 3),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> RingLeftCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 7),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 9),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 5),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 2),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 3),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 10),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 3),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> EarringRightCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 6),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 12),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 1),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 18),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 15),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 6),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 10000, 0),
                new(ResourcesId.WispsWind, 1, 20),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 4),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 1, 14),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 3),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 1),
                new(ResourcesId.WispsWind, 1, 18),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 1, 9),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 4),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 1),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 18),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 8),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 5, 6),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 5, 2),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> EarringLeftCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 2),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 9),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 3),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 11),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 1, 5),
                new(ResourcesId.SilveryWater, 10000, 0),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 17),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 1),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 7),
                new(ResourcesId.SoulsStones, 1, 2),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 19),
                new(ResourcesId.WispsWind, 10000, 0),
                new(ResourcesId.SilveryWater, 1, 4),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 1),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 14),
                new(ResourcesId.WispsWind, 1, 1),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 1, 3),
                
                new(ResourcesId.FrozenFlame, 5, 3),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 3),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 1, 5),
                
                new(ResourcesId.FrozenFlame, 5, 4),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 15),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 10),
                new(ResourcesId.SoulsStones, 1, 7),
                
                new(ResourcesId.FrozenFlame, 5, 6),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            }
        };
        private Dictionary<EquipmentColor, List<Cell>> NecklaceCellCoef = new()
        {
            [EquipmentColor.White] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 4),
                new(ResourcesId.WispsWind, 1, 2),
                new(ResourcesId.SilveryWater, 1, 6),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Orange] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Pink] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 6),
                new(ResourcesId.WispsWind, 1, 4),
                new(ResourcesId.SilveryWater, 1, 8),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Red] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 9),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 14),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 10000, 0),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Green] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 11),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 12),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Blue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 13),
                new(ResourcesId.WispsWind, 1, 8),
                new(ResourcesId.SilveryWater, 1, 16),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 10000, 0),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 1),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.DarkBlue] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 7),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 17),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 10000, 0),
                new(ResourcesId.ScatteringDenseFog, 5, 2),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            },
            [EquipmentColor.Purple] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 11),
                new(ResourcesId.WispsWind, 1, 7),
                new(ResourcesId.SilveryWater, 1, 18),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 3),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Black] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 17),
                new(ResourcesId.WispsWind, 1, 9),
                new(ResourcesId.SilveryWater, 1, 18),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 5, 2),
                new(ResourcesId.LoopingSmoke, 5, 1),
                new(ResourcesId.ScatteringDenseFog, 5, 5),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
                
            },
            [EquipmentColor.Omnicolor] = new List<Cell>()
            {
                new(ResourcesId.FirePowder, 1, 19),
                new(ResourcesId.WispsWind, 1, 10),
                new(ResourcesId.SilveryWater, 1, 20),
                new(ResourcesId.SoulsStones, 10000, 0),
                
                new(ResourcesId.FrozenFlame, 5, 3),
                new(ResourcesId.LoopingSmoke, 5, 2),
                new(ResourcesId.ScatteringDenseFog, 5, 8),
                new(ResourcesId.NonSolidifyingMagma, 10000, 0),
            }
        };
        
        public List<DataCoefs> allCellCoef = new();

        public DataCells()
        {
            allCellCoef.Add(new DataCoefs("Носорог", RinoCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Ястреб", SokolCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Бабочка", ButterflyCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Кобра", CobraCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Дельфин", DolphinCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Лисица", FoxCellCoef, EquipmentSlot.Spirit));
            allCellCoef.Add(new DataCoefs("Верхняя одежда", OuterwearCellCoef, EquipmentSlot.OuterWear));
            allCellCoef.Add(new DataCoefs("Нижняя одежда", UnderwearCellCoef, EquipmentSlot.UnderWear));
            allCellCoef.Add(new DataCoefs("Обувь", FootwearCellCoef, EquipmentSlot.FootWear));
            allCellCoef.Add(new DataCoefs("Кольцо (п)", RingRightCellCoef, EquipmentSlot.Ring));
            allCellCoef.Add(new DataCoefs("Кольцо (л)", RingLeftCellCoef, EquipmentSlot.Ring));
            allCellCoef.Add(new DataCoefs("Серьга (п)", EarringRightCellCoef, EquipmentSlot.EarRing));
            allCellCoef.Add(new DataCoefs("Серьга (л)", EarringLeftCellCoef, EquipmentSlot.EarRing));
            allCellCoef.Add(new DataCoefs("Ожерелье", NecklaceCellCoef, EquipmentSlot.Necklace));
        }
    }
}