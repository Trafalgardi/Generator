﻿using System.Collections.Generic;

namespace TestProject.DataClasses
{
    public class Data
    {
        /// <summary>
        /// Сумма необходимых единиц ресурса «паутина арахны» внутри каждого цвета определяется из двух параметров:
        /// коэффициент снаряжения * коэффициент цвета 1
        /// </summary>
        public Dictionary<EquipmentSlot, int> EquipmentCoef;
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<EquipmentColor, int> ColorCoef1;
        /// <summary>
        /// Количество необходимых единиц ресурса «паутина арахны» для перехода на следующий уровень цвета
        /// определяется из двух параметров: коэффициент снаряжения * коэффициент цвета 2 
        /// </summary>
        public Dictionary<EquipmentColor, int> ColorCoef2; 
        public Dictionary<EquipmentColor, int> ColorCoef3;
        public Dictionary<EquipmentColor, int> CellsCount;

        public Data()
        {
            CellsCount = new Dictionary<EquipmentColor, int>
            {
                {EquipmentColor.White, 4},
                {EquipmentColor.Orange, 6},
                {EquipmentColor.Pink, 8},
                {EquipmentColor.Red, 10},
                {EquipmentColor.Green, 12},
                {EquipmentColor.Blue, 15},
                {EquipmentColor.DarkBlue, 18},
                {EquipmentColor.Purple, 21},
                {EquipmentColor.Black, 25},
                {EquipmentColor.Omnicolor, 30},
            };
            EquipmentCoef = new Dictionary<EquipmentSlot, int>
            {
                {EquipmentSlot.Spirit, 6},
                {EquipmentSlot.OuterWear, 5},
                {EquipmentSlot.UnderWear, 4},
                {EquipmentSlot.FootWear, 3},
                {EquipmentSlot.Ring, 2},
                {EquipmentSlot.EarRing, 4},
                {EquipmentSlot.Necklace, 6}
            };
            ColorCoef1 = new Dictionary<EquipmentColor, int>
            {
                {EquipmentColor.White, 2},
                {EquipmentColor.Orange, 3},
                {EquipmentColor.Pink, 4},
                {EquipmentColor.Red, 5},
                {EquipmentColor.Green, 6},
                {EquipmentColor.Blue, 7},
                {EquipmentColor.DarkBlue, 9},
                {EquipmentColor.Purple, 11},
                {EquipmentColor.Black, 14},
                {EquipmentColor.Omnicolor, 19},
            };
            ColorCoef2 = new Dictionary<EquipmentColor, int>
            {
                {EquipmentColor.White, 1},
                {EquipmentColor.Orange, 1},
                {EquipmentColor.Pink, 1},
                {EquipmentColor.Red, 1},
                {EquipmentColor.Green, 1},
                {EquipmentColor.Blue, 2},
                {EquipmentColor.DarkBlue, 2},
                {EquipmentColor.Purple, 3},
                {EquipmentColor.Black, 4},
                {EquipmentColor.Omnicolor, 6},
            };
            ColorCoef3 = new Dictionary<EquipmentColor, int>
            {
                {EquipmentColor.White, 2},
                {EquipmentColor.Orange, 3},
                {EquipmentColor.Pink, 4},
                {EquipmentColor.Red, 5},
                {EquipmentColor.Green, 6},
                {EquipmentColor.Blue, 7},
                {EquipmentColor.DarkBlue, 8},
                {EquipmentColor.Purple, 9},
                {EquipmentColor.Black, 10},
                //Поменял коэф было 6
                {EquipmentColor.Omnicolor, 10},
            };
        }
    }
}