﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;

namespace TestProject
{
    public enum EquipmentSlot
    {
        Unknown,
        [Display(Name = "Дух")]
        Spirit,
        [Display(Name = "Верхняя одежда")]
        OuterWear,
        [Display(Name = "Нижнее белье")]
        UnderWear,
        [Display(Name = "Ботинки")]
        FootWear,
        [Display(Name = "Кольца")]
        Ring,
        [Display(Name = "Серьги")]
        EarRing,
        [Display(Name = "Ожерелье")]
        Necklace
    }
    public enum EquipmentColor
    {
        Unknown,
        White,
        Orange,
        Pink,
        Red,
        Green,
        Blue,
        DarkBlue,
        Purple,
        Black,
        Omnicolor,
    }
    public enum ResourcesId
    {
        Unknown = 0,
        [Display(Name = "Энергетические сгустки")]
        EnergyClots = 1,
        [Display(Name = "Огненный порошок")]
        FirePowder = 2,
        [Display(Name = "Клочки ветра")]
        WispsWind = 3,
        [Display(Name = "Серебристая вода")]
        SilveryWater = 4,
        [Display(Name = "Души камней")]
        SoulsStones = 5,
        [Display(Name = "Застывшее пламя")]
        FrozenFlame = 6,
        [Display(Name = "Россыпь плотного тумана")]
        ScatteringDenseFog = 7,
        [Display(Name = "Петляющий дым")]
        LoopingSmoke = 8,
        [Display(Name = "Незастывающая магма")]
        NonSolidifyingMagma = 9,
        
        [Display(Name = "Паутина арахны")]
        ArachneWeb = 10,
        [Display(Name = "Неповинующаяся кислота")]
        DefiantAcid = 11,
        [Display(Name = "Нестабильные цветы")]
        UnstableFlowers = 12,
        [Display(Name = "Мурчащий катализатор")]
        PurringCatalyst = 13,
        [Display(Name = "Камни призыва")]
        SummonRocks = 14,
        [Display(Name = "Книги вариаций")]
        BooksVariations = 15,
        [Display(Name = "Голубика забвения")]
        BlueberriesOblivion = 16,
        [Display(Name = "Оранжевые эссенции")]
        OrangeEssences = 17,
        [Display(Name = "Розовые эссенции")]
        PinkEssences = 18,
        [Display(Name = "Красные эссенции")]
        RedEssences = 19,
        [Display(Name = "Зеленые эссенции")]
        GreenEssences = 20,
        [Display(Name = "Голубые эссенции")]
        BlueEssences = 21,
        [Display(Name = "Темносиние эссенции")]
        DarkBlueEssences = 22,
        [Display(Name = "Фиолетовые эссенции")]
        PurpleEssences = 23,
        [Display(Name = "Чёрные эссенции")]
        BlackEssences = 24,
        [Display(Name = "Всецветные эссенции")]
        AllColorEssences = 25,
        [Display(Name = "Концентрат гармонии")]
        HarmonyConcentrate = 26,
        [Display(Name = "Гармоничная вода")]
        HarmoniousWater = 27,
        [Display(Name = "Нота дисгармонии")]
        NoteDisharmony = 28,
        [Display(Name = "Пыль скорби")]
        DustSorrow = 29,
        [Display(Name = "Воющая горошина")]
        HowlingPea = 30
    }
}