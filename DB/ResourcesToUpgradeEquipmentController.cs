﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TestProject.DB
{
    public class ResourcesToUpgradeEquipmentController : IDisposable
    {
        private readonly ResourcesToUpgradeEquipmentContext _context;
        
        public ResourcesToUpgradeEquipmentController()
        {
            _context = new ResourcesToUpgradeEquipmentContext();
        }
        
        public void Dispose()
        {
            _context.SaveChanges();
            _context.Dispose();
        }

        public void DeleteAll()
        {
            _context.Database.ExecuteSqlRaw("TRUNCATE TABLE resources_to_upgrade_equipment");
        }
        
        public void AddRecord(ResourcesToUpgradeEquipment record)
        {
            _context.resources_to_upgrade_equipment.Add(record);
        }
        
        public IEnumerable<ResourcesToUpgradeEquipment> GetAllRecords()
        {
            return _context.resources_to_upgrade_equipment.AsEnumerable();
        }

        public ResourcesToUpgradeEquipment GetRecords(int equipmentColor, int equipmentSharpening, int equipmentBase)
        {
            return _context.resources_to_upgrade_equipment.First(r =>
                r.equipment_color == equipmentColor &&
                r.equipment_sharpening == equipmentSharpening &&
                r.equipment_base == equipmentBase);
        }
    }
}