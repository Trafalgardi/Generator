﻿using System.ComponentModel.DataAnnotations;

namespace TestProject.DB
{
    public class ResourcesToUpgradeEquipment
    {
        [Key] public int Id { get; set; }
        public int equipment_color { get; set; }
        public int equipment_sharpening { get; set; }
        public int equipment_base { get; set; }
        public int fire_powder { get; set; }
        public int wisps_wind { get; set; }
        public int silvery_water { get; set; }
        public int souls_stones { get; set; }
        public int frozen_flame { get; set; }
        public int looping_smoke { get; set; }
        public int scattering_dense_fog { get; set; }
        public int non_solidifying_magma { get; set; }
        public int arachne_web { get; set; }

        public ResourcesToUpgradeEquipment(int equipment_color, int equipment_sharpening, int equipment_base,
            int fire_powder, int wisps_wind, int silvery_water, int souls_stones, int frozen_flame, int looping_smoke,
            int scattering_dense_fog, int non_solidifying_magma, int arachne_web)
        {
            this.equipment_color = equipment_color;
            this.equipment_sharpening = equipment_sharpening;
            this.equipment_base = equipment_base;
            this.fire_powder = fire_powder;
            this.wisps_wind = wisps_wind;
            this.silvery_water = silvery_water;
            this.souls_stones = souls_stones;
            this.frozen_flame = frozen_flame;
            this.looping_smoke = looping_smoke;
            this.scattering_dense_fog = scattering_dense_fog;
            this.non_solidifying_magma = non_solidifying_magma;
            this.arachne_web = arachne_web;
        }
        
        public ResourcesToUpgradeEquipment Set(int equipment_color, int equipment_sharpening, int equipment_base,
            int fire_powder, int wisps_wind, int silvery_water, int souls_stones, int frozen_flame, int looping_smoke,
            int scattering_dense_fog, int non_solidifying_magma, int arachne_web)
        {
            this.equipment_color = equipment_color;
            this.equipment_sharpening = equipment_sharpening;
            this.equipment_base = equipment_base;
            this.fire_powder = fire_powder;
            this.wisps_wind = wisps_wind;
            this.silvery_water = silvery_water;
            this.souls_stones = souls_stones;
            this.frozen_flame = frozen_flame;
            this.looping_smoke = looping_smoke;
            this.scattering_dense_fog = scattering_dense_fog;
            this.non_solidifying_magma = non_solidifying_magma;
            this.arachne_web = arachne_web;

            return this;
        }

        public ResourcesToUpgradeEquipment() { }
    }
}