﻿using Microsoft.EntityFrameworkCore;

namespace TestProject.DB
{
    public class ResourcesToUpgradeEquipmentContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;UserId=mmorpguser;Password=MMO15932159rpg;database=mmorpg_resources;");
        }
        
        public DbSet<ResourcesToUpgradeEquipment> resources_to_upgrade_equipment { get; set; }
    }
}